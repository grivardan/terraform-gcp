resource "google_cloud_run_service" "run_service" {
  name     = "app-dev"
  location = var.region
  project  = var.project_name
  template {
    spec {
      containers {
        image = "gcr.io/cloudrun/hello"
          ports {
          container_port = 3001
        }
      }
    }
  }
  traffic {
    percent         = 100
    latest_revision = true
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}


