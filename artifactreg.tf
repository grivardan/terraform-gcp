resource "google_artifact_registry_repository" "artifact-registry" {
  location      = var.region
  project       = var.project_name
  repository_id = "web-app-dev"
  format        = "DOCKER"
}